<?php namespace Decoupled\Core\Extension;

use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\Extension\Action\ActionExtension;
use Decoupled\Core\Extension\DependencyInjection\DependencyInjectionExtension;
use Decoupled\Core\Extension\Event\EventExtension;
use Decoupled\Core\Extension\Bundle\BundleExtension;
use Decoupled\Core\Extension\State\StateRouterExtension;
use Decoupled\Core\Extension\Scope\ScopeExtension;
use Decoupled\Core\Extension\Output\OutputExtension;
use Decoupled\Core\Bundle\Process\ConfigProcess;
use Decoupled\Core\Bundle\Process\DependencyInjectionProcess;
use Decoupled\Core\Bundle\Process\StateRoutingProcess;
use Decoupled\Core\Bundle\Process\ViewProcess;

class CoreExtension extends ApplicationExtension{

    public function getName()
    {
        return 'core.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        //register base extensions

        $app->uses(
            new ActionExtension(),
            new DependencyInjectionExtension(),
            new EventExtension(),
            new BundleExtension(),
            new StateRouterExtension(),
            new ScopeExtension(),
            new OutputExtension()
        );

        //register bundle process
        
        $app->uses(function( $bundleInitializer ){

            $bundleInitializer->uses(
                new ConfigProcess($this),
                new DependencyInjectionProcess($this),
                new StateRoutingProcess($this),
                new ViewProcess($this)
            );
        });
    }
}