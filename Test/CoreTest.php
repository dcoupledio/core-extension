<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\CoreExtension;

class ExtTest extends TestCase{

    public function testCanUseExtension()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new CoreExtension() );

        $app->when( '$app.run' )->uses(function( $scope ){

            $scope['foo'] = 1;
        });

        return $app;
    }

    /**
    * @depends testCanUseExtension
    **/

    public function testCanDispatchEvent( $app )
    {
        $app->dispatch( '$app.run' );

        $this->assertEquals( $app['scope']['foo'], 1 );
    }

}